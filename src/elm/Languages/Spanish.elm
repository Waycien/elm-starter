module Languages.Spanish exposing (..)

units: List String 
units = [
            "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho",
            "nueve", "diez", "once", "doce", "trece", "catorce", "quince",
            "dieciséis", "diecisiete", "dieciocho", "diecinueve"
        ]

tens: List String   
tens = ["", "", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa"]

scales: List String
scales =  ["cien", "mil", "millón", "mil millones", "trillón", "cuatrillón"]

-- list of words/letters that are ignored
fillerWords: List String
fillerWords = ["y"]