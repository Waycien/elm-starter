module Languages.English exposing (..)

units: List String 
units = [
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
            "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
            "sixteen", "seventeen", "eighteen", "nineteen"
        ]

tens: List String   
tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

scales: List String
scales =  ["hundred", "thousand", "million", "billion", "trillion", "quadrillion"]

-- list of words that are ignored
fillerWords: List String
fillerWords = ["and"]