module GoogleTranslate exposing (Translation, translate, toEnglish, toTargetLang)
import Http
import Array exposing (..)
import Json.Decode exposing (..)


-- Translates text from a specific language into another specific language
translate
    : (Result Http.Error Translation -> msg)
    -> String
    -> String
    -> String
    -> Cmd msg
translate message sourceLang targetLang sourceText  =
    Http.send message <|
        Http.get ("https://translate.googleapis.com/translate_a/single?client=gtx&sl=" 
            ++ sourceLang ++ "&tl=" ++ targetLang ++ "&dt=t&q=" ++ (Http.encodeUri sourceText)) 
            (translationDecoder targetLang)



-- Translate any language recieved into english
toEnglish : (Result Http.Error Translation -> msg) -> String -> Cmd msg
toEnglish message sourceText  =
    translate message "auto" "en" sourceText 


-- Convert any language to a given target language
toTargetLang : (Result Http.Error Translation -> msg) -> String -> String -> Cmd msg
toTargetLang message targetLanguage sourceText  =
    translate message "auto" targetLanguage sourceText 



type alias Translation =
    { translated: String 
    , original:   String 
    , confidence: Float
    , sourceLanguage: String 
    , targetLanguage: String 
    }


{-| Given an index in an array, attempt to decode the value at that location using the
    provided decoder. 
-}
getAndDecode : Int -> Decoder a -> Array Value -> Maybe a
getAndDecode index decoder arr =
    case (Array.get index arr) of -- get object at index in aray
        Just v -> 
            case decodeValue decoder v of -- attempt to decode the object at that index
                Ok x -> Just x 
                Err _ -> Nothing
        Nothing -> Nothing  



-- Given just an array, decode certain values at specific points into a Translation object 
translationDecoder: String ->  Decoder Translation
translationDecoder targetLanguage =
  array value 
    |> andThen (\arr ->  

        let 
            -- extract variables
            decodedNestedArray  = getAndDecode 0 (array (array value)) arr 
            decodedLanguage     = getAndDecode 2 string arr 
            decodedConfidence   = getAndDecode 6 float arr 
        in 

        -- ensure that all values were decoded correctly
        case ( decodedNestedArray, decodedLanguage, decodedConfidence ) of 
            (Just nestedArray, Just language, Just confidence ) -> 
                
                -- pattern match to extract the nested array containing the summary info
                case Array.get 0 nestedArray  of         
                    Just summary -> 
                            
                        let 
                            -- Extract all values from the summary array
                            decodedTranslation  = getAndDecode 0 string summary
                            decodedText         = getAndDecode 1 string summary
                            decodedCount        = getAndDecode 4 int summary
                        in 
                        
                        -- Ensure those summary values all exist and were decoded successfully
                        case (decodedTranslation, decodedText, decodedCount) of 
                            (Just translation, Just text, Just wordCount) ->

                                -- At this point, we have everything we need and return the Translation object
                                succeed   
                                    { translated = translation     
                                    , original = text           
                                    , confidence = confidence
                                    , sourceLanguage = language
                                    , targetLanguage = targetLanguage
                                    }
                        
                            _ -> fail ("Missing information in the Translation array" ++ toString summary)

                    Nothing -> fail "No summary object found"  

            _ -> fail ("Missing vital information needed to decode the response. " 
                        ++ "Translation object: " ++ toString decodedNestedArray 
                        ++ ", Language: " ++ toString decodedLanguage 
                        ++ ", confidence: " ++ toString decodedConfidence )
       
    )
  

