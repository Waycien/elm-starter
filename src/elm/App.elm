module App exposing (..)

import Html
import Html exposing (..)
import Html.Attributes exposing ( placeholder )
import Html.Events exposing (onClick, onInput)
import WordsToNumbers exposing (..)

import Http

import GoogleTranslate exposing (toEnglish)


type alias Model =
    { converter: WordsToNumbers.Model
    , txt: String 
    }


init : ( Model, Cmd Msg )
init =
    ({ converter = WordsToNumbers.init
    , txt = ""
    }
    , Cmd.none )



-- UPDATE


type Msg
    = Translate String  
    | Response (Result Http.Error GoogleTranslate.Translation )
    | Change String 
    | PrepareWords String 
    | WordsToConvert (Result Http.Error GoogleTranslate.Translation )



update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of

        Change str -> 
            ( { model | txt = str }, Cmd.none )



        PrepareWords str ->
            (model, toEnglish WordsToConvert str )

        WordsToConvert (Ok summary) ->
            let 
                _ = Debug.log "parsed" (parseWords summary.translated model.converter)
            in
            (model, Cmd.none)

        WordsToConvert (Err msg) -> 
            (model, Cmd.none)




        Translate str ->
            (model, toEnglish Response str )

        Response (Ok summary) -> 
            let 
                _= summary 
                    |> Debug.log "summary" 
            in 
                    
            ( model, Cmd.none )

        Response (Err msg) -> 
            let _ = Debug.log "error" msg in 
            ( model, Cmd.none )




-- VIEW


view : Model -> Html Msg
view model =
    div [ ]
        [ input [ placeholder "Text to translate", onInput Change ] 
                []
        , button [ onClick (Translate model.txt) ] 
            [ text "Translate" ]

        , button [ onClick (PrepareWords model.txt) ]
            [ text "Words 2 Numbers"]
        ]
