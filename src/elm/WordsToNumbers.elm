module WordsToNumbers exposing (Model, Config, Language, init, initWithConfig, parseWords)
import Dict exposing (..)
import Maybe exposing (andThen, withDefault)

import Languages.English as English


-- Supported Languages
type Language 
    = English

-- Optional Configuation type
type alias Config =
    { language: Language
    }

-- Model 
type alias Model =
    { config: Config
    , wordConverter: Dict String (Int, Int) 
    }



-- Initialize our model (default case)
init : Model 
init =
    let defaultConfig = { language = English } in -- default language is english
    { config = defaultConfig 
    , wordConverter = wordsToNumsDict defaultConfig 
    } 



-- Initialize our model with a custom config object
initWithConfig: Config -> Model 
initWithConfig inputConfig =
    { config = inputConfig
    , wordConverter = wordsToNumsDict inputConfig 
    } 



-- Create our master dictionary containing the words mapped out to a (scale, value)
wordsToNumsDict: Config -> Dict String (Int, Int)
wordsToNumsDict config =

    let 
        (fillerWords, units, tens, scales) =
            case config.language of 
                English -> (English.fillerWords, English.units, English.tens, English.scales )
    in
    empty
        |> union ( fromList (fillerWords  |> List.map (\ element -> (element, (1, 0)) )) )
        |> union ( fromList (units  |> List.indexedMap (\ index element -> (element, (1, index)) )) )
        |> union ( fromList (tens   |> List.indexedMap (\ index element -> (element, (1, index * 10)) )) )
        |> union (createScales scales)
        



createScales: List String -> Dict String (Int, Int)
createScales scales =
    empty 
        |> union    ( fromList  
                            ( scales 
                                |> List.indexedMap 
                                    (\ index element -> 
                                        let 
                                            adjustedIndex   = if index == 0 then 1 else index
                                            multiplier      = if index == 0 then 2 else 3 
                                            value           = 10 ^ (adjustedIndex * multiplier)
                                        in 
                                        (element, (value, 0)) 
                                    )
                            ) 
                    )


-- Customize the text pre-processing based on the language 
applyLanguageRules: Config -> String -> List String 
applyLanguageRules config input =
    case config.language of 
        English ->
                input 
                    |> String.toLower   -- make sure string is lower cased
                    |> String.words     -- split string into a list of words, splitting on chunks of whitespace 
                    |> List.concatMap (\word -> String.split "-" word ) -- break up any hyphenated words  



-- Convert Text to Integers 
parseWords: String -> Model -> Int 
parseWords input model =

    let 
        -- initial accumulator's value with the format: (currentValue, result)
        acc = (0,0) 
        
        -- our function used to help reduce the string
        concatFunction = 
            (\word accumulator -> 
                
                let
                    -- Deconstruct accumulator to get access to the 'current' value
                    (sum, result) = accumulator 

                    -- Try looking up work in the dict
                    lookup = get word model.wordConverter
                in 

                lookup 
                    |> andThen  (\(scale, increment) -> 
                                    let current = sum * scale + increment in 

                                    if scale > 100 then 
                                        Just (0, result + current)
                                    else 
                                        Just (current, result)
                                )
                                
                    |> withDefault accumulator -- don't alter the accumulator if the lookup fais
                
            ) 
        
        (current, result)  = input 
                                |> applyLanguageRules model.config
                                |> List.foldl concatFunction acc
    in 

    result + current

