module CoerciveDecoders exposing (cString, cBool, cInt, cFloat)

import Json.Decode exposing (..)

{-| Coerce any JSON primitive (string, int, float, bool) into a String.

    import Json.Decode exposing (decodcString, list)
    import Json.Decode.Extra exposing (cString)

    """ [ "hello", 3, 10.2, true  ] """
        |> decodcString (list cString) 
    --> Ok [ "hello", "3", "10.2", "true"]
-}
cString: Decoder String
cString =
    oneOf 
        [ string 
        , int   |> map toString 
        , float |> map toString 
        , bool  |> map (\x -> if x then "true" else "false")  
        ]

{-| Coerce any "bool-like" JSON value into a Bool.

    import Json.Decode exposing (decodcString, list)
    import Json.Decode.Extra exposing (cBool)

    """ [ true, "true", " TrUe ", 1, 1.0, "1", "1.0", "01", "01.00" ] """
        |> decodcString (list cBool) 
    --> Ok [ True, True, True, True, True, True, True, True, True ]


    """ [ false, "false", " FALse ", 0, 0.0, "0", "0.0", "00", "00.00" ] """
        |> decodcString (list cBool)  
    --> Ok [ False, False, False, False, False, False, False, False, False ]
-}
cBool: Decoder Bool 
cBool =
    oneOf
        [ bool 
        , int  |> andThen (\val ->
                            case val of
                                1 -> succeed True
                                0 -> succeed False
                                _ -> fail <| "Expecting 1 or 0 but found: " ++ toString val
                            )

        , float  |> andThen (\val ->
                            case val of
                                1.0 -> succeed True
                                0.0 -> succeed False
                                _ -> fail <| "Expecting 1.0 or 0.0 but found: " ++ toString val
                            )
        
        , string |> andThen(\val ->
                            case val |> String.trim |>String.toLower of
                                "true"  -> succeed True
                                "false" -> succeed False
                                _ -> fail <| """Expecting string representations of booleans like: "true" or "false" (case & leading/trailing whitespace doesn't matter) but found: """ ++ val
                            )

        -- This handles ints and floats inside strings like "1" or "1.0"
        , trimParseFloat |> andThen(\val -> 
                                if val == 1.0 then 
                                    succeed True 
                                else if val == 0.0 then 
                                    succeed False 
                                else 
                                    fail <| "Expecting int/float representations of booleans like: (1, 0) or (1.0, 0.0)  but found: " ++ toString val 
                            )
        ]


{-| Coerce any "int-like" JSON value into a Int.

    import Json.Decode exposing (decodcString, list)
    import Json.Decode.Extra exposing (cInt)

    """ [ " 1 ", 2, "3.9", 4.4, "05.000" ] """
        |> decodcString (list cInt) 
    --> Ok [1, 2, 3, 4, 5]


    """ [ " -1 ", -2, "-3.9", -4.4, "-05.000" ] """
        |> decodcString (list cInt) 
    --> Ok [ -1, -2, -3, -4, -5]
-}
cInt: Decoder Int 
cInt = 
    oneOf
        [ int                                       -- Try the default decoder first
        , float |> map truncate                     -- converts 4.6 -> 4
        , bool  |> map (\b -> if b then 1 else 0)   -- convert true -> 1 and false -> 0
        , trimParseInt                              -- converts "3" -> 3
        , trimParseFloat |> map truncate            -- converts "2.8" -> 2
        ]


{-| Coerce any "float-like" JSON value into a Float.

    import Json.Decode exposing (decodcString, list)
    import Json.Decode.Extra exposing (cFloat)

    """ [ " 1.1 ", 2.9, "3.9", 4.4, "05.000" ] """
        |> decodcString (list cFloat)   
    --> Ok [1.1, 2.9, 3.9, 4.4, 5.0]

    """ [ " -1.1 ", -2.9, "-3.9", -4.4, "-05.000" ] """
        |> decodcString (list cFloat)   
    --> Ok [ -1.1, -2.9, -3.9, -4.4, -5.0]
-}
cFloat: Decoder Float 
cFloat =
    oneOf
        [ float                                          -- Try the default decoder first
        , int   |> map toFloat                           -- converts ints to floats
        , bool  |> map (\b -> if b then 1.0 else 0.0)    -- convert true to 1.0 and false to 0.0
        , trimParseFloat                                 -- converts "1.0" -> 1.0
        , trimParseInt |> map toFloat                    -- converts "1" -> 1.0
        ]



{-| Extract a int by first calling [`String.trim`](http://package.elm-lang.org/packages/elm-lang/core/latest/String#trim)
    and piping the string to [`String.toInt`](http://package.elm-lang.org/packages/elm-lang/core/latest/String#toInt)
    
    import Json.Decode exposing (..)
    """ { "field": " 123   " } """
        |> decodcString (field "field" trimParseInt)
    --> Ok 123
-}
trimParseInt : Decoder Int
trimParseInt =
    string |> andThen (String.trim >> String.toInt >> fromResult)


{-| Extract a float by first calling [`String.trim`](http://package.elm-lang.org/packages/elm-lang/core/latest/String#trim) and piping the string to 
    [`String.toFloat`](http://package.elm-lang.org/packages/elm-lang/core/latest/String#toFloat)

    import Json.Decode exposing (..)
    """ { "field": "        50.5 " } """
        |> decodcString (field "field" trimParseFloat)
    --> Ok 50.5
-}
trimParseFloat : Decoder Float
trimParseFloat =
    string |> andThen (String.trim >> String.toFloat >> fromResult)


{-| Transform a result into a decoder

Sometimes it can be useful to use functions that primarily operate on
`Result` in decoders. An example of this is `Json.Decode.Extra.date`. It
uses the built-in `Date.fromString` to parse a `String` as a `Date`, and
then converts the `Result` from that conversion into a decoder which has
either already succeeded or failed based on the outcome.

    import Json.Decode exposing (..)


    validatcString : String -> Result String String
    validatcString input =
        case input of
            "" ->
                Err "Empty string is not allowed"
            _ ->
                Ok input


    """ "something" """
        |> decodcString (string |> andThen (fromResult << validatcString))
    --> Ok "something"


    """ "" """
        |> decodcString (string |> andThen (fromResult << validatcString))
    --> Err "I ran into a `fail` decoder: Empty string is not allowed"

-}
fromResult : Result String a -> Decoder a
fromResult result =
    case result of
        Ok successValue ->
            succeed successValue

        Err errorMessage ->
            fail errorMessage