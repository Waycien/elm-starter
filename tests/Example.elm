module Example exposing (..)

import Expect exposing (Expectation)
import Test exposing (..)

-- -- import Fuzz exposing (list, int, tuple, string)
-- import SimpleDecoders as Simple
-- import Json.Decode exposing (Decoder, list, decodeString)
-- import Result exposing (..)

-- boolDecoderTests : Test
-- boolDecoderTests =
--     describe "Test Decoding Json vaues into booleans"
--         [ test "decoding a normal array of bools into a list of bools" <|
--             \() ->
--                  testDecoder (Json.Decode.list Simple.bool) """[true,false,true]""" (Ok [True, False, True])
       
--         , test "decoding an array strings into a list of bools" <|
--             \() ->
--                  testDecoder (Json.Decode.list Simple.bool) """["true","false","true"]""" (Ok [True, False, True])
        
--         , test "decoding an array of strings with different capitalizaionts into a list of bools" <|
--             \() ->
--                  testDecoder (Json.Decode.list Simple.bool) """["false", "falsE","True","FALSE","TrUe"]""" (Ok [False, False, True, False, True])
        
--         , test "decoding an array strings (with whitespace) into a list of bools" <|
--         \() ->
--                 testDecoder (Json.Decode.list Simple.bool) """["true   ","   false ","    true"]""" (Ok [True, False, True])
    
--         ]


-- intDecoderTests : Test
-- intDecoderTests =
--     describe "Test Decoding Json vaues into ints"
--         [ test "decoding a normal array of numbers into a list of ints" <|
--             \() ->
--                  testDecoder (Json.Decode.list Simple.int) """[1,2,3]""" (Ok [1,2,3])
            
--         , test "decoding a list of floats into ints" <|
--             \() ->
--                  testDecoder (Json.Decode.list Simple.int) """[1.3,2.9,3.0]""" (Ok [1,2,3])


--         , test "decoding a list of floats inside strings into ints" <|
--             \() ->
--                  testDecoder (Json.Decode.list Simple.int) """["1.3","9.9","3.0"]""" (Ok [1,9,3])

        
--         , test "decoding a list of ints inside strings into ints" <|
--             \() ->
--                  testDecoder (Json.Decode.list Simple.int) """["1","2","3"]""" (Ok [1,2,3])

--         , test "decoding a mix of values  strings into a list of ints" <|
--                 \() ->
--                     testDecoder (Json.Decode.list Simple.int) """["1",2,"3.9",4.4]""" (Ok [1,2,3,4])

--         ]

-- -- A helper function used to test a decoder and return an Expectation type
-- testDecoder : Decoder a -> String -> Result String a -> Expectation
-- testDecoder decoder input expected =
--     Expect.equal expected (decodeString decoder input) 